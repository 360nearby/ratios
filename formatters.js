// Format v1.0.0
// var formatted = {
//     from: 'dollar',
//     to: 'bitcoin',
//     cost: '1943.00'
// };
// 
// Format v1.0.1
// 

var formatters = {};

module.exports = function format_data(raw, source) {
    try {
        // Find required price by defined in config depth path, like example ["btc_usd", "last"], raw["btc_usd"]["last"] etc.
        var current = raw;
        var depth = 0;
        while (true) {
            var depth_path = source.jsonPath[depth];
            if (!depth_path && depth_path !== 0) {
                break;
            }
            current = current[depth_path];
            if (!current) {
                break;
            }
            depth++;
        }

        if (!current) {
            console.log('Price not found in data:', raw);
            return;
        }

        var formatted = {
            name: source.exchangeName,
            from: source.from,
            to: source.to,
            cost: current
        };

        // console.log(source);
        // console.log(raw);
        // console.log(formatted);
        // process.exit();

        return formatted;
    } catch (e) {
        console.log('Failed to format source', source, e);
    }
};