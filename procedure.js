// Abbreviations
// (FGS) - Gathering server abbreviation (Current app.js)
// (AG) - Aggregation server abbreviation
// (BS) - Broadcasting server abbreviation

// Native modules

// External modules
var request = require('request');
var async = require('async');

// App modules
var format_data = require('./formatters');



var singleton = {};

singleton.download = function download(opts, cb) {
    request(opts, cb);
}

singleton.parse = function parse(body) {
    try {
        var data = JSON.parse(body);
    } catch (e) {
        console.log('Failed to parse data.', e);
        return;
    }

    return data;
}

singleton.format = function format(raw, source) {
    // console.log('Ready to format data:', raw);
    return format_data(raw, source);
}

singleton.create_procedure = function create_procedure(source) {
    var state = {
        source: source,
        timings: {}
    };

    var stack = [];
    // Download data
    stack.push(function download_task(callback) {
        var opts = {
            url: state.source.url
        };
        state.timings['downloading'] = new Date().getTime();
        singleton.download(opts, function download_cb(error, response, body) {
            state.timings['downloading'] = new Date().getTime() - state.timings['downloading'];
            if (error) {
                console.log('Failed to download data from source', opts.url);
                return;
            }
            state.body = body;
            callback(null);
        });
    });

    // Parse data
    stack.push(function parse_task(callback) {
        state.timings['parsing'] = new Date().getTime();
        state.raw_data = singleton.parse(state.body);
        state.timings['parsing'] = new Date().getTime() - state.timings['parsing'];
        callback(null);
    });

    // Parse data
    stack.push(function format_task(callback) {
        state.timings['formatting'] = new Date().getTime();
        state.formatted = singleton.format(state.raw_data, state.source);
        state.timings['formatting'] = new Date().getTime() - state.timings['formatting'];
        callback(null, state);
    });

    return stack;
}

singleton.launch_procedure = function launch_procedure(stack, cb) {
    async.waterfall(stack, function(error, state) {
        // console.log('Final state:', state);
        // console.log('Task finished:');
        // console.log(state);
        if (cb) cb(error, state);
    });
}

module.exports = singleton;