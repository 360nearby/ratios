var config = {
  websocket: {
    port: 8080
  },
  sources: {

    // \u0243 - bitcoin in Unicode
    // Dollar to Bitcoin price sources

    "BitStampUSD": {
      "exchangeName": "BitStamp",
      "url": "https://www.bitstamp.net/api/v2/ticker/btcusd/",
      "jsonPath": ["last"],
      "from": "dollar",
      "to": "bitcoin",
    },

    "BTCeUSD": {
      "exchangeName": "BTCe",
      "url": "https://btc-e.com/api/3/ticker/btc_usd",
      "jsonPath": ["btc_usd", "last"],
      "from": "dollar",
      "to": "bitcoin",
    },

    "KrakenUSD": {
      "exchangeName": "Kraken",
      "url": "https://api.kraken.com/0/public/Ticker?pair=XBTUSD",
      "jsonPath": ["result", "XXBTZUSD", "c", 0],
      "from": "dollar",
      "to": "bitcoin"
    },

    "CoinDeskUSD": {
      "exchangeName": "CoinDesk",
      "url": "http://api.coindesk.com/v1/bpi/currentprice.json",
      "jsonPath": ["bpi", "USD", "rate_float"],
      "from": "dollar",
      "to": "bitcoin"
    },

    "CoinbaseUSD": {
      "exchangeName": "Coinbase",
      "url": "https://coinbase.com/api/v1/currencies/exchange_rates",
      "jsonPath": ["btc_to_usd"],
      "from": "dollar",
      "to": "bitcoin"
    },

    // \u20ac - euro in Unicode
    // Euro to Bitcoin price sources

    "BTCeEUR": {
      "exchangeName": "BTCe",
      "url": "https://btc-e.com/api/3/ticker/btc_eur",
      "jsonPath": ["btc_eur", "last"],
      "from": "euro",
      "to": "bitcoin"
    },
    "KrakenEUR": {
      "exchangeName": "Kraken",
      "from": "euro",
      "to": "bitcoin",
      "url": "https://api.kraken.com/0/public/Ticker?pair=XBTEUR",
      "jsonPath": ["result", "XXBTZEUR", "c", 0]
    },
    "CoinDeskEUR": {
      "exchangeName": "CoinDesk",
      "from": "euro",
      "to": "bitcoin",
      "url": "http://api.coindesk.com/v1/bpi/currentprice.json",
      "jsonPath": ["bpi", "EUR", "rate_float"]
    },
    "CoinbaseEUR": {
      "exchangeName": "Coinbase",
      "from": "euro",
      "to": "bitcoin",
      "url": "https://coinbase.com/api/v1/currencies/exchange_rates",
      "jsonPath": ["btc_to_eur"]
    },
    "BitPayEUR": {
      "exchangeName": "BitPay",
      "from": "euro",
      "to": "bitcoin",
      "url": "https://bitpay.com/api/rates",
      "jsonPath": [2, "rate"]
    },

    // Euro to dollar price sources
    "fixer.io": {
      "exchangeName": "fixer.io",
      "from": "euro",
      "to": "dollar",
      "url": "https://api.fixer.io/latest",
      "jsonPath": ["rates", "USD"]
    },


  }
}

module.exports = config;