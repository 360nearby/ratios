angular.module('ratesApp').service('ratesService', [function() {
    return function ratesService(on_data){
        var socket = new WebSocket(config.websocket.host);

        socket.onopen = function() {
            console.log("Connection is opened");
        };
        socket.onclose = function(event) {

        };
        socket.onmessage = function(event) {
            try {
                var data = JSON.parse(event.data);
            } catch (e) {
                console.error('Failed to parse websocket message json:', e);
                return;
            }

            on_data(data);
        };
        socket.onerror = function(evt) {

        };
    }
}]);