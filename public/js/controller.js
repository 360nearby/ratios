angular.module('ratesApp').controller('ratesController', ['$scope', '$rootScope', 'ratesService', function($scope, $rootScope, ratesService) {

    $scope.rates={};
    $scope.timings={};

    ratesService(function on_data(res){
        console.log('Received new data:',res);

        $scope.$apply(function(){
            var key=res.data.from+'2'+res.data.to;
            $scope.rates[key]=res.data;

            res.timings.name=res.data.name;
            $scope.timings[res.data.name]=res.timings;
        });
    });
}]);