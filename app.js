// App modules
var cfg = require('./config');
var app = require('./procedure');

// External modules
const WebSocket = require('ws');



// Create Websocket Server
var connections = [];
const wss = new WebSocket.Server(cfg.websocket);
console.log('Server is listening');

wss.on('connection', function connection(ws) {
    ws.on('message', function incoming(message) {
        // console.log('received: %s', message);
    });
});

wss.broadcast = function broadcast(data) {
    var data = JSON.stringify(data);
    wss.clients.forEach(function each(client) {
        if (client.readyState === WebSocket.OPEN) {
            client.send(data);
        }
    });
};

console.log('Websocket server is listening on port:', cfg.websocket.port);

// Bootstrap
var tasks = 0;
function loop() {
    for (var i in cfg.sources) {
        if (tasks > 100) {
            console.log('Tasks limit reached');
            break;
        }
        var source = cfg.sources[i];

        // Can be queued in RabbitMQ/Gearman or any other queue server
        // just pass source to create a procedure

        // Create a procedure
        var procedure = app.create_procedure(source);
        tasks++;
        // Or queue it for later in current server runtime queue
        // or
        // Do it now
        app.launch_procedure(procedure, function(error, state) {
            tasks--;
            console.log('==================================');
            console.log('Active tasks:', tasks);
            console.log(state.timings);
            console.log(state.formatted);

            // Broadcast to clients
            wss.broadcast({
                data:state.formatted,
                timings:state.timings
            });
        });
    }
}
setInterval(loop, 3000);
loop();